import os
import csv

# Set the directory containing the raw CSV files
raw_dir = '../flight_status_pred'

# Set the names of the columns to extract from each CSV file
column_names = [ 'Flight_Number_Operating_Airline','DOT_ID_Operating_Airline', 'Tail_Number', 'OriginAirportID', 'DestAirportID', 'CRSDepTime', 'DepTime', 'CRSArrTime', 'ArrTime', 'AirTime', 'Distance', 'Year', 'Quarter', 'Month', 'DayofMonth', 'DayOfWeek', 'TaxiOut', 'TaxiIn', 'WheelsOff', 'WheelsOn', 'ArrTimeBlk', 'DepTimeBlk', 'DistanceGroup']

updated_column_names = ['FlightNumber','AirlineID', 'TailNumber', 'OriginAirportID', 'DestinationAirportID', 'CRSDepartureTime', 'DepartureTime', 'CRSArrivalTime', 'ArrivalTime', 'AirTime', 'Distance', 'Year', 'Quarter', 'Month', 'DayOfMonth', 'DayOfWeek', 'TaxiOut', 'TaxiIn', 'WheelsOff', 'WheelsOn', 'ArrivalTimeBlock', 'DepartureTimeBlock', 'DistanceGroup']

# Set the name of the output CSV file
output_file = '../csvFiles/FlightMetrics.csv'


id_column = 'MetricID'


# # Set the maximum number of rows to write to the output CSV file
# max_rows = 200000


# Open the output CSV file in write mode
with open(output_file, 'w', newline='') as out_csv:
    writer = csv.writer(out_csv)

    updated_column_names.insert(0,id_column)
    # Write the header row to the output CSV file
    writer.writerow(updated_column_names)

    id_counter = 1
    rows_written = 0

    # Loop through all files in the raw directory
    for filename in os.listdir(raw_dir):
        if filename.startswith('Combined_Flights_') and filename.endswith('.csv'):
            # Open the current CSV file in read mode
            # filename = 'Combined_Flights_2022.csv'
            with open(os.path.join(raw_dir, filename), 'r', newline='') as in_csv:
                reader = csv.reader(in_csv)

                # Get the indices of the columns to extract
                header_row = next(reader)
                column_indices = [header_row.index(name) for name in column_names]

                # Loop through each row in the CSV file and extract the columns
                for row in reader:

                    # if rows_written == max_rows:
                    #     break
                    column_values = [row[i].replace(",", " ") for i in column_indices]
                    column_values.insert(0,id_counter)
                    writer.writerow(column_values)
                    id_counter += 1
                    rows_written += 1

