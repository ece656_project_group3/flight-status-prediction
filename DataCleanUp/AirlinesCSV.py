import os
import csv

# Set the directory containing the raw CSV files
raw_dir = '../flight_status_pred'

# Set the names of the columns to extract from each CSV file
column_names = ['DOT_ID_Operating_Airline','IATA_Code_Operating_Airline', 'Airline']

updated_column_names = ['AirlineID', 'IATACode', 'AirlineName']

# Set the name of the output CSV file
output_file = '../csvFiles/AirlinesData.csv'

# Open the output CSV file in write mode
with open(output_file, 'w', newline='') as out_csv:
    writer = csv.writer(out_csv)

    # Write the header row to the output CSV file
    writer.writerow(updated_column_names)

    unique_rows = set()

    # Loop through all files in the raw directory
    for filename in os.listdir(raw_dir):
        if filename.startswith('Combined_Flights_') and filename.endswith('.csv'):
           
           
    # Open the current CSV file in read mode
    # filename = 'Combined_Flights_2022.csv'
            with open(os.path.join(raw_dir, filename), 'r', newline='') as in_csv:
                reader = csv.reader(in_csv)

                # Get the indices of the columns to extract
                header_row = next(reader)
                column_indices = [header_row.index(name) for name in column_names]

                # Loop through each row in the CSV file and extract the columns
                for row in reader:
                    column_values = [row[i].replace(",", " ") for i in column_indices]
                    unique_rows = tuple(column_values)
                    
                    # If the row is not already in the set, add it and write it to the output CSV file
                    if unique_row not in unique_rows:
                        unique_rows.add(unique_row)
                        writer.writerow(column_values)
