# Imports that will be used to setup the dataset.

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

# Loading the dataset
filepaths = [
             '../flight_status_pred/Combined_Flights_2018.csv',
             '.../flight_status_pred/Combined_Flights_2019.csv',
             '.../flight_status_pred/Combined_Flights_2020.csv',
             '.../flight_status_pred/Combined_Flights_2021.csv',
             '.../flight_status_pred/Combined_Flights_2022.csv']

df_list = []
for filepath in filepaths:
    df = pd.read_csv(filepath)
    df_list.append(df)
df = pd.concat(df_list, axis=0)


# select the first 1 million rows
# to perform a operation on time and based on laptop it can be changed or commented.
# df = df[:1000000]

# Display the first few rows of the dataset
print(df.head())

# Check for missing values
print(df.isnull().sum())

# Check for outliers
sns.boxplot(x=df['DepDelay'])

print(df.columns.tolist())

# Remove unnecessary columns
df = df.drop(['FlightDate','Cancelled', 'Diverted', 'CRSDepTime','Tail_Number', 'DepTime', 'ArrTime', 'ArrDelay', 'AirTime', 'CRSElapsedTime', 'ActualElapsedTime', 'Marketing_Airline_Network', 'Operated_or_Branded_Code_Share_Partners', 'DOT_ID_Marketing_Airline', 'IATA_Code_Marketing_Airline', 'Flight_Number_Marketing_Airline', 'Operating_Airline', 'DOT_ID_Operating_Airline', 'IATA_Code_Operating_Airline', 'Flight_Number_Operating_Airline', 'OriginAirportSeqID', 'OriginCityMarketID', 'OriginStateFips', 'OriginWac', 'DestAirportSeqID', 'DestCityMarketID', 'DestStateFips', 'DestWac', 'DepDel15', 'DepartureDelayGroups', 'DepTimeBlk', 'TaxiOut', 'WheelsOff', 'WheelsOn', 'TaxiIn', 'ArrDel15', 'ArrivalDelayGroups', 'ArrTimeBlk', 'DistanceGroup', 'DivAirportLandings', 'OriginCityName', 'OriginState', 'OriginStateName', 'DestCityName','DestState', 'DestStateName'], axis=1)

# Handle missing values
df = df.dropna()

# Encode categorical variables
from sklearn.preprocessing import LabelEncoder
le = LabelEncoder()
df['Origin'] = le.fit_transform(df['Origin'])
df['Dest'] = le.fit_transform(df['Dest'])
df['Airline'] = le.fit_transform(df['Airline'])

# Splitting dataset into the training and testing where 30% of the total data will go in testing.
from sklearn.model_selection import train_test_split
X = df.drop(['DepDelay'], axis=1)
y = df['DepDelay']
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=42)

# Transforming the data to fit the model with StandardScaler
from sklearn.preprocessing import StandardScaler
scaler = StandardScaler()
X_train = scaler.fit_transform(X_train)
X_test = scaler.transform(X_test)

# Creating a Random Forest Classifier to classify the flight related data.
from sklearn.ensemble import RandomForestRegressor
rf = RandomForestRegressor(n_estimators=100, random_state=42)
rf.fit(X_train, y_train)

# Creating a Mean Squared Error metrics to find the error ratio.
from sklearn.metrics import mean_squared_error
y_pred = rf.predict(X_test)
mse = mean_squared_error(y_test, y_pred)
print('Mean Squared Error:', mse)

# Creating a graph for the predicted dalay of the flight vs actual flight delay
plt.scatter(y_test, y_pred)
plt.xlabel('Actual Delay')
plt.ylabel('Predicted Delay')
plt.title('Random Forest Regression')
plt.show()



