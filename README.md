# Flight status prediction

## **Description**

## Requirements
- MySQL server
- Python

## Setup and Installation steps
Here we have the dataset in the local directory so, we'll perform all the operations on that only.


- Go to the `DataCleanUp` directory
- Run all the python files.
    For example : `py AirlinesCSV.py` -> This would generate the csv file in `csvFiles` directory.
    This would generate the csv files according to our need of the database to directl input in the MySQL server. This is useful to handle operations. 

- Go to the `MySQLFiles` and now we need to run `CreateTablesWithData.sql` file in the mysql.
    This will generate all the required tables in the mysql and load the data into it. We have already created a data for this in the above step. 

- Go to the `PythonFiles` directory and run `py Main.py`.
    This will run our client application to execute all queries and related functionalities throught the client interactions.

- Go to the `DataMining` and Run python file to perform thr DataMining excercise.


