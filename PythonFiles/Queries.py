''' SQL Queries to retrieve all data from the MySQL server.
    All queries should be stored here.
'''
SQL_Queries = {
    'AIRPORT_NAME_QUERY': "SELECT AirportName, CityName FROM Airports WHERE StateName =%s;",
    'SEARCH_FLIGHT_QUERY': """
                            SELECT DISTINCT(Flights.FlightNumber), Airlines.AirlineName, Airports1.AirportName AS OriginAirportName, Airports2.AirportName AS DestinationAirportName, FlightMetrics.DepartureTime, FlightMetrics.ArrivalTime
                            FROM Flights
                            INNER JOIN Airlines ON Flights.AirlineID = Airlines.AirlineID
                            INNER JOIN FlightMetrics ON Flights.FlightNumber = FlightMetrics.FlightNumber
                            INNER JOIN Airports AS Airports1 ON FlightMetrics.OriginAirportID = Airports1.AirportID
                            INNER JOIN Airports AS Airports2 ON FlightMetrics.DestinationAirportID = Airports2.AirportID
                            WHERE Airports1.AirportName = %s
                            AND Airports2.AirportName = %s
                            AND FlightMetrics.Year = %s
                            AND FlightMetrics.Month = %s
                            AND FlightMetrics.DayOfMonth = %s;
                            """,
    'BUSIEST_AIRPORTS_QUERY': """
                                SELECT Airports.AirportName, Airports.CityName, COUNT(DISTINCT(FlightMetrics.FlightNumber)) as TotalFlights
                                FROM FlightMetrics
                                INNER JOIN Airports ON FlightMetrics.DestinationAirportID = Airports.AirportID
                                GROUP BY Airports.AirportName,Airports.CityName
                                ORDER BY TotalFlights DESC
                                LIMIT 10;
                                """,
    'LIST_AIRLINES_QUERY': "SELECT AirlineName FROM Airlines;",
    'SEARCH_FLIGHTS_ON_TIME_QUERY': """
                            SELECT DISTINCT(Flights.FlightNumber)
                            FROM Flights
                            INNER JOIN FlightMetrics ON Flights.FlightNumber = FlightMetrics.FlightNumber
                            INNER JOIN Airlines ON Flights.AirlineID = Airlines.AirlineID
                            INNER JOIN FlightStatus AS FS ON Flights.FlightNumber = FS.FlightNumber
                            WHERE Airlines.AirlineName like %s
                            AND FS.ArrivalDelayMinutes = 0;
                            """,
    'LIST_TOTAL_FLIGHTS_BY_AIRLINE_IN_A_YEAR_QUERY': """
                            SELECT COUNT(DISTINCT(Flights.FlightNumber)) AS TotalFlights
                            FROM Flights
                            INNER JOIN FlightMetrics AS FM ON Flights.FlightNumber = FM.FlightNumber
                            INNER JOIN Airlines ON Airlines.AirlineID = Flights.AirlineID
                            WHERE Airlines.AirlineName LIKE %s
                            AND FM.Year = %s;
                            """,
    'CALCULATE_PERCENTAGE_OF_FLIGHTS_ARRIVED_WITHIN_15_MINS_QUERY': """
                            SELECT Airlines.AirlineID, Airlines.AirlineName, (SUM(CASE WHEN ArrDel15 = 1 THEN 1 ELSE 0 END) / COUNT(*))*100 AS PercentageOnTime
                            FROM FlightStatus
                            INNER JOIN Airlines ON FlightStatus.AirlineID= Airlines.AirlineID
                            GROUP BY Airlines.AirlineID;
                            """,
    'CALCULATE_AVERAGE_DEPARTURE_DELAY_OF_AIRLINE_IN_A_MONTH_QUERY': """
                            SELECT AVG(DepartureDelayMinutes) AS AvgDepartureDelay
                            FROM FlightStatus
                            JOIN FlightMetrics AS FM ON FlightStatus.FlightNumber = FM.FlightNumber AND FlightStatus.AirlineID = FM.AirlineID
                            JOIN Airlines ON FlightStatus.AirlineID= Airlines.AirlineID
                            WHERE Airlines.AirlineName like %s
                            AND FM.Month = %s;
                            """,
    'FIND_AIRLINES_WITH_HIGHEST_PERCENTAGE_OF_DELAYED_FLIGHTS_QUERY': """
                            SELECT A.AirlineName,
                            COUNT(*) AS TotalFlights,
                            COUNT(CASE WHEN FS.DepDel15 = 1 THEN 1 END) AS DelayedFlights,
                            COUNT(CASE WHEN FS.DepDel15 = 1 THEN 1 END) * 100.0 / COUNT(*) AS DelayedPercentage
                            FROM Flights F
                            JOIN Airlines a ON F.AirlineID = A.AirlineID
                            JOIN FlightStatus FS ON F.FlightNumber = FS.FlightNumber
                            GROUP BY A.AirlineName
                            ORDER BY DelayedPercentage DESC
                            LIMIT 10;
                            """,
    'ADD_AIRLINE_QUERY':"""
                            INSERT INTO Airlines (AirlineID, IATACode, AirlineName) VALUES (%s, %s, %s);
                            """,
    'UPDATE_DEPARTURE_DELAY_QUERY':"""
                            UPDATE FlightStatus
                            SET DepartureDelayMinutes = %s
                            WHERE FlightNumber = %s;
                            """,
    'DELETE_CANCELLED_FLIGHT_QUERY':"""
                            DELETE FROM FlightStatus
                            WHERE FlightNumber = %s
                            AND Cancelled=TRUE;
                            """
}
