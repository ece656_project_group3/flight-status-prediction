import os
from SQLConnector import *
from tabulate import tabulate
from Queries import SQL_Queries

def add_airline():
    # Add new airline
    os.system('cls')
    print("Enter the Airline details:")
    airline_id = input('Enter the Airline ID:')
    IATA_code = input('Enter IATA Code:')
    airline_name = input('Enter the Airline Name:')

    try:
        values = [airline_id, IATA_code, airline_name]
        mycursor.execute(SQL_Queries['ADD_AIRLINE_QUERY'], values)
        mydb.commit()
        print("Airline added successfully!")
    except mysql.connector.Error as error:
        print(f"Error adding airline: {error}")
        mydb.rollback()
    mydb.close()
    input("Press any key to continue..")

def update_departure_delay():
    # Update the departure delay for a particular flight
    os.system('cls')
    print("Enter the Flight details:")
    flight_number = input('Enter the Flight Number:')
    departure_delay = input('Enter the Departure Delay:')

    try:
        values = [departure_delay, flight_number]
        mycursor.execute(SQL_Queries['UPDATE_DEPARTURE_DELAY_QUERY'], values)
        mydb.commit()
        print("Departure delay updated successfully!")
    except mysql.connector.Error as error:
        print(f"Error updating departure delay: {error}")
        mydb.rollback()
    input("Press any key to continue..")

def delete_cancelled_flight():
    # Delete a cancelled flight from the FlightStatus table
    os.system('cls')
    print("Enter the Flight details:")
    flight_number = input('Enter the Flight Number:')

    try:
        values = [flight_number]
        mycursor.execute(SQL_Queries['DELETE_CANCELLED_FLIGHT_QUERY'], values)
        mydb.commit()
        print("Cancelled flight deleted successfully!")
    except mysql.connector.Error as error:
        print(f"Error deleting cancelled flight: {error}")
        mydb.rollback()
    input("Press any key to continue..")

def admin_menu():
    # Admin menu
    os.system('cls')
    print("------Admin Menu--------")
    print("1: Add new Airlines.")
    print("2: Update the departure delay for a particular flight.")
    print("3: Delete a cancelled flight from the FlightStatus table.")
    print("0: Go back to main menu.")

    opt = int(input("Please select one option:"))
    if (opt == 1):
        add_airline()
    elif (opt == 2):
        update_departure_delay()
    elif (opt == 3):
        delete_cancelled_flight()
    elif (opt == 0):
        return
    else:
        print("Invalid option selected!")
        input("Press any key to continue..")