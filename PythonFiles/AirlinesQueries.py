import os
from SQLConnector import *
from tabulate import tabulate
from Queries import SQL_Queries

def list_airlines():
    # List airlines
    
    # Execute the query
    mycursor.execute(SQL_Queries['LIST_AIRLINES_QUERY'])

    # Get the results
    myresult = mycursor.fetchall()

    # Print the results
    columns = [desc[0] for desc in mycursor.description]
    print(tabulate(myresult, headers=columns, tablefmt="grid"))
    input("Press any key to continue..")

def search_flights_on_time():
    # Search flights of a specific airline that are on time
    # Get the airline name from the user
    airline_name = input('Enter the airline name:')

    values = ['%'+airline_name+'%']

    # Execute the query
    mycursor.execute(SQL_Queries['SEARCH_FLIGHTS_ON_TIME_QUERY'], values)

    # Get the results
    myresult = mycursor.fetchall()

    # Print the results
    columns = [desc[0] for desc in mycursor.description]
    print(tabulate(myresult, headers=columns, tablefmt="grid"))
    input("Press any key to continue..")

def list_total_flights_by_airline_in_a_year():
    # List total flights oprated by specific airline in a given year
    # Get the airline name from the user
    airline_name = input('Enter the airline name:')

    # Get the year from the user
    year = int(input('Enter the Year (YYYY):'))

    values = ['%'+airline_name+'%', year]

    # Execute the query
    mycursor.execute(SQL_Queries['LIST_TOTAL_FLIGHTS_BY_AIRLINE_IN_A_YEAR_QUERY'], values)

    # Get the results
    myresult = mycursor.fetchall()

    # Print the results
    columns = [desc[0] for desc in mycursor.description]
    print(tabulate(myresult, headers=columns, tablefmt="grid"))
    input("Press any key to continue..")

def calculate_percentage_of_flights_arrived_within_15_minutes():
    # Calculate the percentage of flights that arrived within 15 minutes of the scheduled arrival time for all Airlines
    
    # Execute the query
    mycursor.execute(SQL_Queries['CALCULATE_PERCENTAGE_OF_FLIGHTS_ARRIVED_WITHIN_15_MINS_QUERY'])

    # Get the results
    myresult = mycursor.fetchall()

    # Print the results
    columns = [desc[0] for desc in mycursor.description]
    print(tabulate(myresult, headers=columns, tablefmt="grid"))
    input("Press any key to continue..")

def calculate_average_departure_delay_of_airline_in_a_month():
    # Calculate the average departure delay of a specific airline in a given month
    # Get the airline name from the user
    airline_name = input('Enter the airline name:')

    # Get the month from the user
    month = int(input('Enter the Month (MM):'))

    values = ['%'+airline_name+'%', month]

    # Execute the query
    mycursor.execute(SQL_Queries['CALCULATE_AVERAGE_DEPARTURE_DELAY_OF_AIRLINE_IN_A_MONTH_QUERY'], values)

    # Get the results
    myresult = mycursor.fetchall()

    # Print the results
    columns = [desc[0] for desc in mycursor.description]
    print(tabulate(myresult, headers=columns, tablefmt="grid"))
    input("Press any key to continue..")

def find_airlines_with_highest_percentage_of_delayed_flights():
    # Find the airlines with highest percentage of delayed flights
    
    # Execute the query
    mycursor.execute(SQL_Queries['FIND_AIRLINES_WITH_HIGHEST_PERCENTAGE_OF_DELAYED_FLIGHTS_QUERY'])

    # Get the results
    myresult = mycursor.fetchall()

    # Print the results
    columns = [desc[0] for desc in mycursor.description]
    print(tabulate(myresult, headers=columns, tablefmt="grid"))
    input("Press any key to continue..")

def airlines_info():
    os.system('cls')
    print("What information do you need related to Airlines?")
    print("1: List all airlines.")
    print("2: Search flights of a specific airline that are on time.")
    print("3: List total flights oprated by specific airline in a given year.")
    print("4: Calculate the percentage of flights that arrived within 15 minutes of the scheduled arrival time for all Airlines.")
    print("5: Calculate the average departure delay of a specific airline in a given month.")
    print("6: Find the airlines with highest percentage of delayed flights.")
    print("0: Go back to main menu.")
    opt = int(input("Please select one option:"))
    if (opt == 0):
        return
    elif (opt == 1):
        list_airlines()
    elif (opt == 2):
        search_flights_on_time()
    elif (opt ==3):
        list_total_flights_by_airline_in_a_year()
    elif (opt == 4):
        calculate_percentage_of_flights_arrived_within_15_minutes()
    elif (opt == 5):
        calculate_average_departure_delay_of_airline_in_a_month()
    elif (opt == 6):
        find_airlines_with_highest_percentage_of_delayed_flights()
    else:
        print("Invalid option selected!")
        input("Press any key to continue..")