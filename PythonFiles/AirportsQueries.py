import os
from SQLConnector import *
from tabulate import tabulate
from Queries import SQL_Queries

def list_airports_in_state():
    # List all airports in a given state
    # Get the state abbreviation from the user
    state_name = input('Enter the state name:')

    values = [state_name]

    ''' 
    This is a sample Query --starts
    '''
    mycursor.execute(SQL_Queries['AIRPORT_NAME_QUERY'], values);
    myresult = mycursor.fetchall()
    columns = [desc[0] for desc in mycursor.description]
    print(tabulate(myresult, headers=columns, tablefmt="grid"))

def search_flight_between_airports():
    # Search for flights between two airports
    # Get the airport Names from the user
    origin_airport_name = input('Enter the origin airport Name:')
    destination_airport_name = input('Enter the destination airport Name:')

    # Get the date from the user
    year = int(input('Enter the Year (YYYY):'))
    month = int(input('Enter the month (MM):'))
    day = int(input('Enter the day (DD):'))

    values = [origin_airport_name, destination_airport_name, year, month, day]

    # Execute the query
    mycursor.execute(SQL_Queries['SEARCH_FLIGHT_QUERY'], values)

    # Get the results
    myresult = mycursor.fetchall()

    # Print the results
    columns = [desc[0] for desc in mycursor.description]
    print(tabulate(myresult, headers=columns, tablefmt="grid"))
    input("Press any key to continue..")

def list_busiest_airports():
    # List the busiest airports

    # Execute the query
    mycursor.execute(SQL_Queries['BUSIEST_AIRPORTS_QUERY'])

    # Get the results
    myresult = mycursor.fetchall()

    # Print the results
    columns = [desc[0] for desc in mycursor.description]
    print(tabulate(myresult, headers=columns, tablefmt="grid"))
    input("Press any key to continue..")


def airports_info():
    os.system('cls')
    print("What information do you need related to Airports?")
    print("1: List all airports in a given state.")
    print("2: Search for flights between two airports.")
    print("3: List top 10 busiest airports.")
    print("0: Go back to main menu.")
    opt = int(input("Please select one option:"))
    if (opt == 0):
        return
    elif (opt == 1):
        list_airports_in_state()
        input("Press any key to continue..")
    elif (opt == 2):
        search_flight_between_airports()
        input("Press any key to continue..")
    elif (opt == 3):
        list_busiest_airports()
        input("Press any key to continue..")
    else:
        print("Invalid option selected!")
        input("Press any key to continue..")
