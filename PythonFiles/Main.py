import os

from SQLConnector import *
from AirportsQueries import *
from AirlinesQueries import *
from AdminQueries import *

# Actions for each User selections
def main_menu_input(opt):
    if(opt==1):
        airports_info()
    elif(opt==2):
        airlines_info()
    elif(opt==3):
        admin_menu()
    else:
        print("Invalid option selected!")
        input("Press any key to continue..")

# Main Menu Options code
if __name__=="__main__":
    if(mydb.is_connected()):
        print("Connected to the server..")
        while (1):
            try:
                os.system('cls')
                print("------Welcome to AirFlight Dataset--------")
                print("Options:")
                print("1: Airports")
                print("2: Airlines")
                print("3: Admin-Menu")
                print("0: Exit!!")
                opt = int(input("Please select one option:"))
                if (opt == 0):
                    exit()
                else:
                    main_menu_input(opt)
            except ValueError:
                os.system('cls')
                print("Oops! Wrong input..")
    else:
        print("Not connected")