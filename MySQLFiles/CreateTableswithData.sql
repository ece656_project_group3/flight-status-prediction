USE airflightdb;

-- to enable load data commnad  
SET GLOBAL local_infile=1;

DROP TABLE IF  EXISTS Flights;
DROP TABLE IF  EXISTS flightstatus;
DROP TABLE IF  EXISTS flightmetrics;
DROP TABLE IF  EXISTS airports;
DROP TABLE IF  EXISTS airlines;

CREATE TABLE Airlines (
  AirlineID INT PRIMARY KEY,
  IATACode CHAR(2) DEFAULT NULL,
  AirlineName VARCHAR(255) DEFAULT NULL
);

LOAD DATA LOCAL INFILE 'D:\\UoW\\ECE-656\\project\\csvFiles\\AirlinesData.csv'
INTO TABLE airlines
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;

CREATE TABLE Airports (
  AirportID INT PRIMARY KEY,
  AirportSeqID INT DEFAULT NULL,
  CityMarketID INT DEFAULT NULL,
  AirportName VARCHAR(100) DEFAULT NULL,
  CityName VARCHAR(100) DEFAULT NULL,
  StateCode CHAR(2) DEFAULT NULL,
  StateName VARCHAR(100) DEFAULT NULL,
  StateFips CHAR(2) DEFAULT NULL,
  Wac INT DEFAULT NULL
);

LOAD DATA LOCAL INFILE 'D:\\UoW\\ECE-656\\project\\csvFiles\\AirportsData.csv'
INTO TABLE airports
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;

CREATE TABLE Flights (
  FlightID INT PRIMARY KEY,
  FlightNumber INT,
  AirlineID INT NOT NULL,
  TailNumber VARCHAR(10),
  FOREIGN KEY (AirlineID) REFERENCES Airlines(AirlineID)
);

LOAD DATA LOCAL INFILE 'D:\\UoW\\ECE-656\\project\\csvFiles\\FlightsData.csv'
INTO TABLE flights
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;


CREATE TABLE FlightMetrics (
    MetricID INT PRIMARY KEY,
    FlightNumber INT,
	AirlineID INT NOT NULL,
	TailNumber VARCHAR(10),
    OriginAirportID INT,
    DestinationAirportID INT,
    CRSDepartureTime INT,
    DepartureTime INT,
    CRSArrivalTime INT,
    ArrivalTime INT,
    AirTime INT,    
    Distance INT,
    Year INT, 
    Quarter INT,
    Month INT,
    DayOfMonth INT,
    DayOfWeek INT,
    TaxiOut INT,
    TaxiIn INT,
    WheelsOff INT,
    WheelsOn INT,
    ArrivalTimeBlock VARCHAR(100),
	DepartureTimeBlock VARCHAR(100),
    DistanceGroup INT,
    FOREIGN KEY (OriginAirportID) REFERENCES airports(AirportID),
    FOREIGN KEY (DestinationAirportID) REFERENCES airports(AirportID)
);

LOAD DATA LOCAL INFILE 'D:\\UoW\\ECE-656\\project\\csvFiles\\FlightMetrics.csv'
INTO TABLE FlightMetrics
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;



CREATE TABLE FlightStatus (
	StatusID INT PRIMARY KEY,
    FlightNumber INT,
	AirlineID INT NOT NULL,
	TailNumber VARCHAR(10),
	DepartureDelayMinutes INT,
    DepartureDelay INT,
	ArrivalDelayMinutes INT,
    DepDel15 BOOLEAN,
	DepartureDelayGroups INT,
	ArrivalDelay INT,
    ArrDel15 BOOLEAN,
    ArrivalDelayGroups INT,
    DivAirportLandings INT,
    Cancelled BOOLEAN,
    Diverted BOOLEAN
);

LOAD DATA LOCAL INFILE 'D:\\UoW\\ECE-656\\project\\csvFiles\\FlightStatus.csv'
INTO TABLE FlightStatus
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;



