use airflightdb;
-- Retrive Flight status by flight number and date
SELECT DISTINCT(FlightMetrics.TailNumber), FlightMetrics.OriginAirportID, FlightMetrics.DestinationAirportID, FlightMetrics.DepartureTime, FlightMetrics.ArrivalTime, FlightStatus.DepartureDelayMinutes, FlightStatus.ArrivalDelayMinutes, FlightStatus.DepartureDelay, FlightStatus.ArrivalDelay, FlightStatus.Cancelled, FlightStatus.Diverted FROM Flights INNER JOIN FlightMetrics ON Flights.FlightNumber = FlightMetrics.FlightNumber INNER JOIN FlightStatus ON Flights.FlightNumber = FlightStatus.FlightNumber WHERE Flights.FlightNumber = 4301 AND FlightMetrics.Year = 2022 AND FlightMetrics.Month = 4 AND FlightMetrics.DayOfMonth = 4;

-- 3.	Get a list of flights departing from a particular airport: 
SELECT Flights.FlightNumber, Flights.TailNumber, Airlines.AirlineName, FlightMetrics.CRSDepartureTime, FlightMetrics.DepartureTime, FlightStatus.DepartureDelayMinutes FROM Flights INNER JOIN Airlines ON Flights.AirlineID = Airlines.AirlineID INNER JOIN FlightMetrics ON Flights.FlightNumber = FlightMetrics.FlightNumber INNER JOIN FlightStatus ON Flights.FlightNumber = FlightStatus.FlightNumber WHERE FlightMetrics.OriginAirportID = 11921 AND FlightMetrics.Year = 2022 AND FlightMetrics.Month = 4 AND FlightMetrics.DayOfMonth = 4 ORDER BY FlightMetrics.CRSDepartureTime ASC;

-- Retrive the list of Airports in a particular state 
SELECT AirportName, CityName, StateCode FROM Airports WHERE StateCode = 'CO';


-- Search flights between two airports with their arrival and departure times on a specific day.
SELECT DISTINCT(Flights.FlightNumber), Airlines.AirlineName, Airports1.AirportName AS OriginAirportName, Airports2.AirportName AS DestinationAirportName, FlightMetrics.DepartureTime, FlightMetrics.ArrivalTime
FROM Flights
INNER JOIN Airlines ON Flights.AirlineID = Airlines.AirlineID
INNER JOIN FlightMetrics ON Flights.FlightNumber = FlightMetrics.FlightNumber
INNER JOIN Airports AS Airports1 ON FlightMetrics.OriginAirportID = Airports1.AirportID
INNER JOIN Airports AS Airports2 ON FlightMetrics.DestinationAirportID = Airports2.AirportID
WHERE Airports1.AirportName = 'ABE'
AND Airports2.AirportName = 'ORD'
AND FlightMetrics.Year = 2022
AND FlightMetrics.Month = 4
AND FlightMetrics.DayOfMonth = 4;


SELECT Airlines.AirlineName, Flights.FlightNumber, FlightStatus.Cancelled, FlightStatus.Diverted
FROM Flights
INNER JOIN Airlines ON Flights.AirlineID = Airlines.AirlineID
INNER JOIN FlightStatus ON Flights.FlightNumber = FlightStatus.FlightNumber
INNER JOIN FlightMetrics ON Flights.FlightNumber = FlightMetrics.FlightNumber
WHERE Airlines.AirlineName like '%Air Wisconsin Airlines Corp%'
AND FlightMetrics.Year = 2022
AND FlightMetrics.Month = 4
AND FlightMetrics.DayOfMonth= 4
AND FlightStatus.Cancelled = 1 ;

SELECT DISTINCT(Flights.FlightNumber)
FROM Flights
INNER JOIN FlightMetrics ON Flights.FlightNumber = FlightMetrics.FlightNumber
INNER JOIN Airlines ON Flights.AirlineID = Airlines.AirlineID
INNER JOIN FlightStatus AS FS ON Flights.FlightNumber = FS.FlightNumber
WHERE Airlines.AirlineName like '%GoJet Airlines  LLC d/b/a United Express%'
AND FS.ArrivalDelayMinutes = 0;

SELECT COUNT(DISTINCT(Flights.FlightNumber)) AS TotalFlights
FROM Flights
INNER JOIN FlightMetrics AS FM ON Flights.FlightNumber = FM.FlightNumber
INNER JOIN Airlines ON Airlines.AirlineID = Flights.AirlineID
WHERE Airlines.AirlineName LIKE '%Commutair Aka Champlain Enterprises  Inc.%' 
  AND FM.Year = 2022;


SELECT Airports.AirportName, Airports.CityName, COUNT(DISTINCT(FlightMetrics.FlightNumber)) as TotalFlights
FROM FlightMetrics
INNER JOIN Airports ON FlightMetrics.DestinationAirportID = Airports.AirportID
-- WHERE Airports.StateName = 'Arizona'
GROUP BY Airports.AirportName,Airports.CityName
ORDER BY TotalFlights DESC
LIMIT 10;


SELECT Airlines.AirlineID, Airlines.AirlineName, SUM(CASE WHEN ArrDel15 = 1 THEN 1 ELSE 0 END) / COUNT(*) AS PercentageOnTime 
FROM FlightStatus 
INNER JOIN Airlines ON FlightStatus.AirlineID= Airlines.AirlineID
GROUP BY Airlines.AirlineID;

SELECT AVG(DepartureDelayMinutes) AS AvgDepartureDelay
FROM FlightStatus
JOIN FlightMetrics AS FM ON FlightStatus.FlightNumber = FM.FlightNumber AND FlightStatus.AirlineID = FM.AirlineID
JOIN Airlines ON FlightStatus.AirlineID= Airlines.AirlineID
WHERE Airlines.AirlineName like "%Republic Airlines%"
AND FM.Month = 4;

CREATE index idx_flightNumber on FlightMetrics(FlightNumber, airlineID); 